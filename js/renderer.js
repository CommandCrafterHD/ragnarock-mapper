// ==================================================================================
// This is where the copyright would be, but honestly most of this code
// is gonna be yoinked from stackoverflow... So I don't really hold much copyright
// over this anyways. :D You'll see what code I have written based on the
// horrible abominations you might find hidden in this file...
// in case you need to know who botched all this together:
//
//          Florian Cegledi (c) 2021
//          contact me at: florian@cegledi.net
//
// ==================================================================================

import * as THREE from './threeJS/src/Three.js';

// Set up the scene, camera, renderer and mouseMesh as global variables.
var scene, camera, renderer, mouseMesh;

// Setup the mouse variables, used to track the mouse position
var mouse = {x: 0, y: 0};
var raycaster = new THREE.Raycaster();
var plane = new THREE.Plane(new THREE.Vector3(0, 1, 0));
var planeIntersect = new THREE.Vector3();

var allHits = {1: [], 2: [], 3: [], 4: []};

// Creating some custom geometry following this simple sheet:
// https://media.cheggcdn.com/study/241/241e363e-7c34-4eef-92c1-99206b9e647a/13156-7.5-44IE2.png
// Also this is mapped in X, Z, -Y (For some reason)
const hexBuffer = new THREE.BufferGeometry();
const hexVerts = new Float32Array( [
  -3.0, 1.0, -3.0,
  -4.0, 1.0, 0.0,
   0.0, 1.0, 0.0,

   0.0, 1.0, -4.0,
   -3.0, 1.0, -3.0,
   0.0, 1.0, 0.0,

   3.0, 1.0, -3.0,
   0.0, 1.0, -4.0,
   0.0, 1.0, 0.0,

   4.0, 1.0, 0.0,
   3.0, 1.0, -3.0,
   0.0, 1.0, 0.0,

   3.0, 1.0, 3.0,
   4.0, 1.0, 0.0,
   0.0, 1.0, 0.0,

   0.0, 1.0, 4.0,
   3.0, 1.0, 3.0,
   0.0, 1.0, 0.0,

   -3.0, 1.0, 3.0,
   0.0, 1.0, 4.0,
   0.0, 1.0, 0.0,

   -4.0, 1.0, 0.0,
   -3.0, 1.0, 3.0,
   0.0, 1.0, 0.0,
] );

// itemSize = 3 because there are 3 values (components) per vertex
hexBuffer.setAttribute( 'position', new THREE.BufferAttribute( hexVerts, 3 ) );
const cursorMat = new THREE.MeshBasicMaterial( { color: 0xff0000, opacity: 0.5, transparent: true } );
const hitMat = new THREE.MeshBasicMaterial( { color: 0xDE935F } );

// quick and dirty clamp function
function clamp(val, min, max) {
    return Math.max(min, Math.min(max, val));
}

// Hey! More quick and dirty functions!
function arrayRemove(arr, value) {
    return arr.filter(function(ele){
        return ele != value;
    });
}


// Update triggered on every movement of the mouse
function onMouseMove(event) {
  var borderElement = document.getElementById('c');

	// Update the mouse variable with the current mouse position
	event.preventDefault();
  mouse.x = ( event.clientX / borderElement.clientWidth ) * 2 - 2;
	mouse.y = - ( event.clientY / borderElement.clientHeight ) * 2 + 1;
  raycaster.setFromCamera(mouse, camera);

 // Make the sphere follow the mouse
  raycaster.ray.intersectPlane(plane, planeIntersect);
  planeIntersect.x = clamp(Math.round(planeIntersect.x), -2, 1);
  planeIntersect.z = clamp(Math.round(planeIntersect.z), -99999, 0);
	mouseMesh.position.copy(planeIntersect);
};

function placeHit() {
  var hitMesh = new THREE.Mesh(hexBuffer, hitMat);
	hitMesh.position.copy(mouseMesh.position);
  hitMesh.scale.set(0.1, 0.1, 0.1);
  switch (mouseMesh.position.x) {
    case -2:
      if (allHits[1].includes(mouseMesh.position.z)) {return;}
      allHits[1].push(mouseMesh.position.z);
      hitMesh.uuid = "1:"+mouseMesh.position.z;
      break;
    case -1:
      if (allHits[2].includes(mouseMesh.position.z)) {return;}
      allHits[2].push(mouseMesh.position.z);
      hitMesh.uuid = "2:"+mouseMesh.position.z;
      break;
    case 0:
      if (allHits[3].includes(mouseMesh.position.z)) {return;}
      allHits[3].push(mouseMesh.position.z);
      hitMesh.uuid = "3:"+mouseMesh.position.z;
      break;
    case 1:
      if (allHits[4].includes(mouseMesh.position.z)) {return;}
      allHits[4].push(mouseMesh.position.z);
      hitMesh.uuid = "4:"+mouseMesh.position.z;
      break;
    default:
      console.error("Well fuck... This ain't supposed to happen chief!");
  }
  scene.add(hitMesh);
}

function deleteHitObject(toDelete) {
  if (toDelete) {
    toDelete.geometry.dispose();
    toDelete.material.dispose();
    scene.remove(toDelete);
  }
}

function removeHit() {
  switch (mouseMesh.position.x) {
    case -2:
      allHits[1] = arrayRemove(allHits[1], mouseMesh.position.z);
      deleteHitObject(scene.getObjectByProperty( 'uuid', "1:"+mouseMesh.position.z ));
      break;
    case -1:
      allHits[2] = arrayRemove(allHits[2], mouseMesh.position.z);
      deleteHitObject(scene.getObjectByProperty( 'uuid', "2:"+mouseMesh.position.z ));
      break;
    case 0:
      allHits[3] = arrayRemove(allHits[3], mouseMesh.position.z);
      deleteHitObject(scene.getObjectByProperty( 'uuid', "3:"+mouseMesh.position.z ));
      break;
    case 1:
      allHits[4] = arrayRemove(allHits[4], mouseMesh.position.z);
      deleteHitObject(scene.getObjectByProperty( 'uuid', "4:"+mouseMesh.position.z ));
      break;
    default:
      console.error("Well fuck... This ain't supposed to happen chief! (Deletion)");
  }
}

// Sets up the scene.
function init() {

  // Create the scene and set the scene size.
  var borderElement = document.getElementById('c');
  scene = new THREE.Scene();
  var WIDTH = borderElement.clientWidth,
      HEIGHT = borderElement.clientHeight;

  c.addEventListener('contextmenu', function(e) {
    e.preventDefault();
    removeHit();
  }, false);

  c.onclick = function(e) {
    placeHit();
  };

  // Create a renderer and add it to the DOM.
  renderer = new THREE.WebGLRenderer({antialias:true});
  renderer.setSize(WIDTH, HEIGHT);
  borderElement.appendChild(renderer.domElement);

  // Create a camera, zoom it out from the model a bit, and add it to the scene.
  camera = new THREE.PerspectiveCamera(45, WIDTH / HEIGHT, 0.1, 20000);
  camera.position.set(-0.5,10,0);
  camera.lookAt(-0.5, 0, 0);
  scene.add(camera);

  // Create an event listener that resizes the renderer with the browser window.
  window.addEventListener('resize', function() {
    var WIDTH = borderElement.clientWidth,
        HEIGHT = borderElement.clientHeight;
    renderer.setSize(WIDTH, HEIGHT);
    camera.aspect = WIDTH / HEIGHT;
    camera.updateProjectionMatrix();
  });

  // Set the background color of the scene.
  renderer.setClearColor(0x333F47, 1);

  var height = 100;

  // Add the horizontal ones
  for (var i = 0; i <= height; i++) {
    const material = new THREE.LineBasicMaterial( { color: 0x81A2BE } );

    const points = [];
    points.push( new THREE.Vector3( -2.5, 0, -i ) );
    points.push( new THREE.Vector3( 1.5, 0, -i ) );

    const geometry = new THREE.BufferGeometry().setFromPoints( points );
    const line = new THREE.Line( geometry, material );

    scene.add( line );
  }

  // Add the vertical lines.
  for (var i = -2; i < 3; i++) {
    const material = new THREE.LineBasicMaterial( { color: 0x5F819D } );

    const points = [];
    points.push( new THREE.Vector3( i-0.5, 0, 0 ) );
    points.push( new THREE.Vector3( i-0.5, 0, -height ) );

    const geometry = new THREE.BufferGeometry().setFromPoints( points );
    const line = new THREE.Line( geometry, material );

    scene.add( line );
  }

  // Create a circle around the mouse and move it
	mouseMesh = new THREE.Mesh(hexBuffer, cursorMat);
	mouseMesh.position.z = -5;
  mouseMesh.scale.set(0.1, 0.1, 0.1);
	scene.add(mouseMesh);

  // Setup an EventListener to trigger when the mouse moves!
	document.addEventListener('mousemove', onMouseMove, false);

  // movement
  var ySpeed = 0.1;
  var ySpeedMod = 5;

  var map = {};
  onkeydown = onkeyup = function(e){
      e = e || event; // to deal with IE
      map[e.keyCode] = e.type == 'keydown';
      if (map[87] && !map[16]) {
        camera.translateY(ySpeed);
      } else if (map[87] && map[16]) {
        camera.translateY(ySpeed*ySpeedMod);
      } else if (map[83] && !map[16]) {
        if (camera.position.y > 10){
          camera.translateY(-ySpeed);
        }
      } else if (map[83] && map[16]) {
        if (camera.position.y > 10) {
          camera.translateY(-ySpeed*ySpeedMod);
        }
      };
  }
}


  // Renders the scene and updates the render as needed.
function animate() {

  // Read more about requestAnimationFrame at http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
  requestAnimationFrame(animate);

  // Render the scene.
  renderer.render(scene, camera);

}

  init();
  animate();
